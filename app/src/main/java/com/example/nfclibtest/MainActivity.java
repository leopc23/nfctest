package com.example.nfclibtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import mx.com.sfinx.nfclib.*;

import java.util.logging.Logger;

/**
 * Created on 23 jun 2019
 *
 * @author Alejandro Escobar - aescobar@sfinx.com.mx
 */
public class MainActivity extends AppCompatActivity {

    private ErrorCodes error = new ErrorCodes ();
    private DireccionesDTOCev setDireccionesDto = new DireccionesDTOCev();
    private DireccionesDTOCev getDireccionesDto = new DireccionesDTOCev();
    private TablaMaestraDTOCev getTablaMaestraDto = new TablaMaestraDTOCev();
    private TablaMaestraDTOCev setTablaMaestraDto = new TablaMaestraDTOCev();
    private VacunadoDTOCev setVacunadoDto= new VacunadoDTOCev();
    private VacunadoDTOCev getVacunadoDto= new VacunadoDTOCev();
    private ResponsableDTOCev getResponsableDto= new ResponsableDTOCev();
    private ResponsableDTOCev setResponsableDto= new ResponsableDTOCev();
    private InfoVacunaNfcDTOCev setInfoVacunaNfcDto = new InfoVacunaNfcDTOCev();
    private InfoVacunaNfcDTOCev getInfoVacunaNfcDto = new InfoVacunaNfcDTOCev();
    private NfcDTOCev setNfcDtoCev = new NfcDTOCev();
    private NfcDTOCev getNfcDtoCev = new NfcDTOCev();





    private VacunadoDTOCev setVacunado = new VacunadoDTOCev();
    private VacunadoDTOCev getVacunado = new VacunadoDTOCev();

    private static final String TAG = "SFINX SA DE CV";

    public static final byte[] TRANSPORT_MFP_AES_KEY =
            {                                                 // AES128 key
                    (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff,
                    (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff,
                    (byte)0xff, (byte)0xff
            };

    public static final byte[] AFILIACION_KEY_AES =
            {                                                 // AES128 key
                    (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA,
                    (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA, (byte)0xAA,
                    (byte)0xAA, (byte)0xAA
            };

    @Override
    protected void  onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int res = SfpApiCev.getInstance().activateLibrary(this);

        Log.i(TAG,"Activate Library Error:"+res);
            if(res!=0)
                Log.e(TAG, "Activate Library Error: " + error.getDescriptionError(res));

    }



    @Override
    public void onNewIntent(final Intent intent){

        MifareTagDetails details = new MifareTagDetails();
        int res = 0;

        super.onNewIntent(intent);

        res = SfpApiCev.getInstance().activateTag(intent, details );

        if(res!=0)
            Log.e(TAG, "ActivateTag Error: " + error.getDescriptionError(res));


        Log.d(TAG, "Mifare Chip Type: " + details.getCardType());
        Log.d(TAG, "Tag UID: " +  details.getUidCard() + " ");
        Log.d(TAG, "Tag SAK: " +  details.getSak() + " ");
        Log.d(TAG, "Tag Security Level: " +  details.getSecurityLevel() + " ");
        Log.d(TAG, "Tag ATQA: " +  details.getAtqa() + " ");
        Log.d(TAG, "Tag Historical Bytes: " +  details.getHistoricalBytes() + " ");
        Log.d(TAG, "Tag Total Memory: " +  details.getTotalMemory() + " ");


        switch (details.getCardType()) {

            case "Plus SL3":{

                int securitySector = 0x4000;

                res=SfpApiCev.getInstance().aesMfpTagAuthentication(SfpApiCev.TRANSPORT_MFP_AES_KEY,securitySector);
                if(res!=0)
                    Log.e(TAG, "aesMfpTagAuthentication Error: " + error.getDescriptionError(res));


            /*    res =SfpApiCev.getInstance().deleteMfpTag();

                if(res!=0)
                    Log.e(TAG, "Delete Error: " + error.getDescriptionError(res));
*/


                //-----------Verificacion de Versiones

                res = SfpApiCev.getInstance().readCevTablaMaestraData(getTablaMaestraDto);
                if(res!=0)
                    Log.e(TAG, "readCevTablaMaestraData Error: " + ErrorCodes.getDescriptionError(res));
                else {

                    System.out.println("readCevTablaMaestraData Ok..........: ");

                    if(!getTablaMaestraDto.getVersionMapaDatos().equals("4.1")){

                        System.out.println("Version 3.1 CEV.......");


                        StringBuilder oldVersionCevData = new StringBuilder();
                        res = SfpApiCev.getInstance().readCevTagOldVersion(oldVersionCevData);


                        System.out.println("OldVersionCev Data: " + oldVersionCevData.toString());

                        break;
                    }else
                        System.out.println("Version 4.1 CEV.......");

                }

                System.out.println("Continua Flujo en Main con version 4.1.......");

                //   ------------------------




                //--------------Direcciones

                setDireccionesDto.setIdDireccion(999999789);
                setDireccionesDto.setIdAgeb(1200);
                setDireccionesDto.setIdLocalicad(876);
                setDireccionesDto.setIdManzana(44);
                setDireccionesDto.setIdMunicipio(2438);
                setDireccionesDto.setIdEstado(33);
                setDireccionesDto.setEstadoValidacion(4);
                setDireccionesDto.setFechaUltimaEdicion("31/06/2019 23:30:40");

              /*  res = SfpApiCev.getInstance().writeCevDirecciones(setDireccionesDto);

                if(res!=0)
                    Log.e(TAG, "WriteCevDirecciones Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevDirecciones OK..........: " );
*/

                res = SfpApiCev.getInstance().readCevDirecciones(getDireccionesDto);

                if(res!=0)
                    Log.e(TAG, "readCevDirecciones Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevDirecciones OK..........: " );


                System.out.println("readCevDirecciones IdDireccion: " + getDireccionesDto.getIdDireccion() );
                System.out.println("readCevDirecciones IdAgeb: " + getDireccionesDto.getIdAgeb() );
                System.out.println("readCevDirecciones IdLocalidad: " + getDireccionesDto.getIdLocalicad() );
                System.out.println("readCevDirecciones IdManzana: " + getDireccionesDto.getIdManzana() );
                System.out.println("readCevDirecciones IdMunicipio: " + getDireccionesDto.getIdMunicipio() );
                System.out.println("readCevDirecciones IdEstado: " + (int)getDireccionesDto.getIdEstado() );
                System.out.println("readCevDirecciones estadoValidacion: " + getDireccionesDto.getEstadoValidacion() );
                System.out.println("readCevDirecciones fechaUltimaValidacion: " + getDireccionesDto.getFechaUltimaEdicion() );


                //-----------------Vacunado ---------------------

                setVacunadoDto.setIdClues(14567);
                setVacunadoDto.setCertificadoNacimiento(9000976);
                setVacunadoDto.setIdNacionalidad(33);
                setVacunadoDto.setIdUsuario(2234455);
                setVacunadoDto.setFechaAutorizacion("08/12/2019 07:14:50");
                setVacunadoDto.setFechaRegistroApp("18/05/2015 16:30:40");
                setVacunadoDto.setFechaRegistroBd("24/06/2016 02:30:05");
                setVacunadoDto.setFechaNacimiento("23/12/1000");
                setVacunadoDto.setIdEstadoNacimiento(15);
                setVacunadoDto.setIdEstadoRegistro(30);
                setVacunadoDto.setIdVacunAccion("AVAC345YUI");
                setVacunadoDto.setEstadoValidacion(3);
                setVacunadoDto.setSexo(1);   // 0 = Mujer 1 = Hombre
                setVacunadoDto.setBidireccional(1);   //  1 - 0

/*

                res = SfpApiCev.getInstance().writeCevVacunadoData(setVacunadoDto);
                if(res!=0)
                    Log.e(TAG, "writeCevVacunadoData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevVacunadoData Ok..........: " );
*/


                setVacunadoDto.setNombre("ANDRES");

//                res = SfpApiCev.getInstance().writeCevVacunadoNombre(setVacunadoDto);
//                if(res!=0)
//                    Log.e(TAG, "WriteCevVacunadoNombre Error: " + ErrorCodes.getDescriptionError(res));
//                else
//                    System.out.println("writeCevVacunadoNombre Ok..........: " );


                setVacunadoDto.setPrimerApellido("FLORES");
                setVacunadoDto.setSegundoApellido("POLICARPO");

//                res = SfpApiCev.getInstance().writeCevVacunadoApellidos(setVacunadoDto);
//                if(res!=0)
//                    Log.e(TAG, "WriteCevVacunadoApellidos Error: " + ErrorCodes.getDescriptionError(res));
//                else
//                    System.out.println("writeCevVacunadoApellidos Ok..........: " );


                res = SfpApiCev.getInstance().readCevVacunadoData(getVacunadoDto);
                if(res!=0)
                    Log.e(TAG, "readCevVacunadoData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevVacunadoData Ok..........: " );

                System.out.println("readCevVacunado IdClues: " + getVacunadoDto.getIdClues() );
                System.out.println("readCevVacunado certificadoNacimiento: " + getVacunadoDto.getCertificadoNacimiento() );
                System.out.println("readCevVacunado idNacionalidad: " + getVacunadoDto.getIdNacionalidad() );
                System.out.println("readCevVacunado idUsuario: " + getVacunadoDto.getIdUsuario() );
                System.out.println("readCevVacunado Fecha Autorizacion: " + getVacunadoDto.getFechaAutorizacion() );
                System.out.println("readCevVacunado Fecha Registro App: " + getVacunadoDto.getFechaRegistroApp() );
                System.out.println("readCevVacunado Fecha Registro BD: " + getVacunadoDto.getFechaRegistroBd() );
                System.out.println("readCevVacunado Fecha Nacimiento: " + getVacunadoDto.getFechaNacimiento() );
                System.out.println("readCevVacunado EstadoNacimiento: " + getVacunadoDto.getIdEstadoNacimiento() );
                System.out.println("readCevVacunado EstadoRegistro: " + getVacunadoDto.getIdEstadoRegistro() );
                System.out.println("readCevVacunado idVacunAccion: " + getVacunadoDto.getIdVacunAccion() );
                System.out.println("readCevVacunado EstadoValidacion: " + getVacunadoDto.getEstadoValidacion() );
                System.out.println("readCevVacunado Sexo: " + getVacunadoDto.getSexo() );
                System.out.println("readCevVacunado Bidireccional: " + getVacunadoDto.getBidireccional() );


                res = SfpApiCev.getInstance().readCevVacunadoNombre(getVacunadoDto);

                if(res!=0)
                    Log.e(TAG, "readCevVacunadoNombre Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevVacunadoNombre Ok..........: " );

                System.out.println("readCevVacunado Nombre: " + getVacunadoDto.getNombre() );

                res = SfpApiCev.getInstance().readCevVacunadoApellidos(getVacunadoDto);

                if(res!=0)
                    Log.e(TAG, "readCevVacunadoApellidos Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevVacunadoApellidos Ok..........: " );

                System.out.println("readCevVacunado PrimerAoellido: " + getVacunadoDto.getPrimerApellido() );
                System.out.println("readCevVacunado SegundoApellido: " + getVacunadoDto.getSegundoApellido() );




                //------------------Responsable-----------------------

                setResponsableDto.setIdNacionalidad(666);
                setResponsableDto.setFechaRegistro("21/02/2016 11:31:34");
                setResponsableDto.setFechaUltimaEdicion("22/03/2015 07:00:00");
                setResponsableDto.setFechaAutorizacion("10/10/2018 14:20:56");
                setResponsableDto.setIdResponsableVac("a25dc184517240f09952a7666acb65f79876");  // sin guiones medios
                setResponsableDto.setIdParentesco(67);
                setResponsableDto.setIdTipo(1);   // 1= CA 0= CE
                setResponsableDto.setEstadoValidacion(4);
                setResponsableDto.setIdOrigenRegistro(21);
                setResponsableDto.setSexo(1);  // 1= hombre 0= mujer
                setResponsableDto.setIdOperador(33);
                setResponsableDto.setIdEstadoNacimiento(18);
                setResponsableDto.setFechaNacimiento("01/01/1980");

                setResponsableDto.setIdResponsableVacunAccion("abbccddeeff88991234567809a56f1223344");  // sin guiones medios
                setResponsableDto.setIdTelefonoVac("11332244556677889900aacddc3e3f2aa3e4");  // sin guiones medios

                setResponsableDto.setNumeroTelefonico("5539991423");

              /*  res = SfpApiCev.getInstance().writeCevResponsableData(setResponsableDto);
                if(res!=0)
                    Log.e(TAG, "writeCevResponsableData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevResponsableData Ok..........: " );
*/

                res = SfpApiCev.getInstance().readCevResponsableData(getResponsableDto);
                if(res!=0)
                    Log.e(TAG, "readCevResponsableData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevResponsableData Ok..........: " );

                System.out.println("readCevResponsable IdNacionalidad: " + getResponsableDto.getIdNacionalidad() );
                System.out.println("readCevResponsable Fecha Registro: " + getResponsableDto.getFechaRegistro() );
                System.out.println("readCevResponsable Fecha UltimaEdicion: " + getResponsableDto.getFechaUltimaEdicion());
                System.out.println("readCevResponsable Fecha Autorizacion: " + getResponsableDto.getFechaAutorizacion());
                System.out.println("readCevResponsable idResponsableVac: " + getResponsableDto.getIdResponsableVac() );
                System.out.println("readCevResponsable idParentesco: " + getResponsableDto.getIdParentesco() );
                System.out.println("readCevResponsable idTipo: " + getResponsableDto.getIdTipo() );
                System.out.println("readCevResponsable estadoVaidacion: " + getResponsableDto.getEstadoValidacion() );
                System.out.println("readCevResponsable idOrigenRegistro: " + getResponsableDto.getIdOrigenRegistro() );
                System.out.println("readCevResponsable Sexo: " + getResponsableDto.getSexo() );
                System.out.println("readCevResponsable idOperador: " + getResponsableDto.getIdOperador() );
                System.out.println("readCevResponsable idEstadoNacimiento: " + getResponsableDto.getIdEstadoNacimiento() );
                System.out.println("readCevResponsable Fecha Nacimiento: " + getResponsableDto.getFechaNacimiento() );

                System.out.println("readCevResponsable idResponsableVacunaccion: " + getResponsableDto.getIdResponsableVacunAccion() );
                System.out.println("readCevResponsable idTelefonoVac: " + getResponsableDto.getIdTelefonoVac() );
                System.out.println("readCevResponsable NumeroTelefonico: " + getResponsableDto.getNumeroTelefonico() );

                setResponsableDto.setNombre("ALEJANDRO");

             /*   res = SfpApiCev.getInstance().writeCevResponsableNombre(setResponsableDto);
                if(res!=0)
                    Log.e(TAG, "writeCevResponsableNombre Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevResponsableNombre Ok..........: " );

*/
                setResponsableDto.setPrimerApellido("SALINAS");
                setResponsableDto.setSegundoApellido("GARCIA");

              /*  res = SfpApiCev.getInstance().writeCevResponsableApellidos(setResponsableDto);
                if(res!=0)
                    Log.e(TAG, "writeCevResponsableApellidos Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevResponsableApellidos Ok..........: " );

*/
                res = SfpApiCev.getInstance().readCevResponsableNombre(getResponsableDto);

                if(res!=0)
                    Log.e(TAG, "readCevResponsableNombre Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevResponsableNombre Ok..........: " );

                System.out.println("readCevResponsable Nombre: " + getResponsableDto.getNombre() );

                res = SfpApiCev.getInstance().readCevResponsableApellidos(getResponsableDto);

                if(res!=0)
                    Log.e(TAG, "readCevResponsableApellidos Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevResponsableApellidos Ok..........: " );

                System.out.println("readCevResponsable PrimerAoellido: " + getResponsableDto.getPrimerApellido() );
                System.out.println("readCevResponsable SegundoApellido: " + getResponsableDto.getSegundoApellido() );


                //----------Tabla Maestra

                setTablaMaestraDto.setIdVacunadoVac("55338899336677889911aacddc3e3f2aa35f");  // sin guiones medios
                setTablaMaestraDto.setFechaUltimaEdicion("31/02/2018 09:31:34");
                setTablaMaestraDto.setIdInstitucion(99);

            /*    res = SfpApiCev.getInstance().writeCevTablaMaestraData(setTablaMaestraDto);
                if(res!=0)
                    Log.e(TAG, "writeCevTablaMaestraData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevTablaMaestraData Ok..........: " );
*/

                //--- Contador Vacunas
/*

                res = SfpApiCev.getInstance().writeCevVaccineNumber(2);
                if(res!=0)
                    Log.e(TAG, "writeCevVaccineNumber Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevVaccineNumber Ok..........: " );

*/


                //--- Version Mapa de Datos

             /*   res = SfpApiCev.getInstance().writeCevDataMapVersion(4,1);
                if(res!=0)
                    Log.e(TAG, "writeCevDataMapVersion Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevDataMapVersion Ok..........: " );

*/

                res = SfpApiCev.getInstance().readCevTablaMaestraData(getTablaMaestraDto);
                if(res!=0)
                    Log.e(TAG, "readCevTablaMaestraData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevTablaMaestraData Ok..........: " );

                System.out.println("readCevTablaMaestra IdVacunadoVac: " + getTablaMaestraDto.getIdVacunadoVac() );
                System.out.println("readCevTablaMaestra FechaUltimaEdicion: " + getTablaMaestraDto.getFechaUltimaEdicion() );
                System.out.println("readCevTablaMaestra Id Institucion: " + getTablaMaestraDto.getIdInstitucion());
                System.out.println("readCevTablaMaestra Contador Vacunas: " + getTablaMaestraDto.getContadorVacunas());
                System.out.println("readCevTablaMaestra Version Mapa de Datos: " + getTablaMaestraDto.getVersionMapaDatos());



                //----------Tabla NFC

                setNfcDtoCev.setFechaBd("10/02/2018 09:31:34");
                setNfcDtoCev.setIdStatus(8);

            /*    res = SfpApiCev.getInstance().writeCevNfcData(setNfcDtoCev);
                if(res!=0)
                    Log.e(TAG, "writeCevNfcData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("writeCevNfcData Ok..........: " );
*/

                res = SfpApiCev.getInstance().readCevNfcData(getNfcDtoCev);
                if(res!=0)
                    Log.e(TAG, "readCevNfcData Error: " + ErrorCodes.getDescriptionError(res));
                else
                    System.out.println("readCevNfcData Ok..........: " );

                System.out.println("readCevNfcData FechaBd: " + getNfcDtoCev.getFechaBd() );
                System.out.println("readCevNfcData IdStatus: " + getNfcDtoCev.getIdStatus());

                //----------------- Info Vacuna NFC ---------------------
                // Remplace middle script from a25dc184-5172-40f0-9952-a7666acb65f7 to get only hex values
                String idVacunadoVacunaVac = "a25dc184-5172-40f0-9952-a7666acb65f7".replace("-", "");
                setInfoVacunaNfcDto.setIdVacunadoVacunaVac("0000"+idVacunadoVacunaVac);
                setInfoVacunaNfcDto.setIdVacuna(195);
                setInfoVacunaNfcDto.setIdClues(8888);
                setInfoVacunaNfcDto.setFechaRegistroBD("31/06/2019 23:30:40");
                setInfoVacunaNfcDto.setFechaAplicacion("31/06/2019");
                setInfoVacunaNfcDto.setIdEquivalencia(7777);
                setInfoVacunaNfcDto.setFechaConsignacionApp("31/06/2019 23:30:55");
                setInfoVacunaNfcDto.setFechaAutorizacion("31/06/2019");
                setInfoVacunaNfcDto.setFechaExpiracion("24/01/2035");
                setInfoVacunaNfcDto.setIdEtapa(1);
                setInfoVacunaNfcDto.setEstadoValidacion(2);
                setInfoVacunaNfcDto.setIdOrigenRegistro(3);
                setInfoVacunaNfcDto.setVacunaHistorica(1); // 1 = true, 0 = false
                setInfoVacunaNfcDto.setDosis(1);
                setInfoVacunaNfcDto.setIdColor(3);

                setInfoVacunaNfcDto.setFechaRegistroBDRespuesta("31/06/2019 23:30:40");
                setInfoVacunaNfcDto.setFechaUltimaEdicion("31/06/2019 23:30:40");
                setInfoVacunaNfcDto.setRespuesta(2);

            /*   res = SfpApiCev.getInstance().writeCevInfoVacunaNfc(setInfoVacunaNfcDto, VaccineBlocksCEV.FIRST_VACCINE_CEV);

                if(res!=0)
                    Log.e(TAG, "WriteCevInfoVacunaNfc Error: " + res);
                else
                    System.out.println("WriteCevInfoVacunaNfc OK..........: " );
*/
               res = SfpApiCev.getInstance().readCevInfoVacunaNfc(getInfoVacunaNfcDto, VaccineBlocksCEV.SECOND_VACCINE_CEV);

                if(res!=0)
                    Log.e(TAG, "readCevInfoVacunaNfc Error: " + res);
                else
                    System.out.println("readCevInfoVacunaNfc OK..........: " );


                System.out.println("readCevInfoVacunaNfc IdVacunadoVacunaVac: " + getInfoVacunaNfcDto.getIdVacunadoVacunaVac() );
                System.out.println("readCevInfoVacunaNfc IdVacuna: " + getInfoVacunaNfcDto.getIdVacuna() );
                System.out.println("readCevInfoVacunaNfc IdClues: " + getInfoVacunaNfcDto.getIdClues() );
                System.out.println("readCevInfoVacunaNfc fechaRegistroBD: " + getInfoVacunaNfcDto.getFechaRegistroBD() );
                System.out.println("readCevInfoVacunaNfc fechaAplicacion: " + getInfoVacunaNfcDto.getFechaAplicacion() );
                System.out.println("readCevInfoVacunaNfc IdEquivalencia: " + getInfoVacunaNfcDto.getIdEquivalencia() );
                System.out.println("readCevInfoVacunaNfc fechaConsignacionApp: " + getInfoVacunaNfcDto.getFechaConsignacionApp() );
                System.out.println("readCevInfoVacunaNfc fechaAutorizacion: " + getInfoVacunaNfcDto.getFechaAutorizacion() );

                System.out.println("readCevInfoVacunaNfc fechaExpiracion: " + getInfoVacunaNfcDto.getFechaExpiracion() );
                System.out.println("readCevInfoVacunaNfc IdEtapa: " + getInfoVacunaNfcDto.getIdEtapa() );
                System.out.println("readCevInfoVacunaNfc EstadoValidacion: " + getInfoVacunaNfcDto.getEstadoValidacion() );
                System.out.println("readCevInfoVacunaNfc IdOrigenRegistro: " + getInfoVacunaNfcDto.getIdOrigenRegistro() );
                System.out.println("readCevInfoVacunaNfc VacunaHistorica: " + getInfoVacunaNfcDto.getVacunaHistorica() );
                System.out.println("readCevInfoVacunaNfc Dosis: " + getInfoVacunaNfcDto.getDosis() );
                System.out.println("readCevInfoVacunaNfc IdColor: " + getInfoVacunaNfcDto.getIdColor() );
                System.out.println("readCevInfoVacunaNfc fechaRegistroDBRespuesta: " + getInfoVacunaNfcDto.getFechaRegistroBDRespuesta() );
                System.out.println("readCevInfoVacunaNfc fechaUltimaEdicion: " + getInfoVacunaNfcDto.getFechaUltimaEdicion() );
                System.out.println("readCevInfoVacunaNfc Respuesta: " + getInfoVacunaNfcDto.getRespuesta() );


                res = SfpApiCev.getInstance().resetNfcFieldMfp();


                break;
            }
            case "MIFARE Classic": {
                StringBuilder response = new StringBuilder();
                res = SfpApiCev.getInstance().readCevMifareClassic(response);
                if(res != ErrorCodes.SFX_EXIT_SUCCESS)
                    Log.e(TAG, ErrorCodes.getDescriptionError(res));
                else
                    Log.d(TAG, "MIFARE CLASSIC DATA: " + response.toString());
                break;
            }
            default:
                break;
        }

        //res = SfpApiCev.getInstance().resetNfcField();

    }

    @Override
    protected void onResume() {
        SfpApiCev.getInstance().startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onPause() {
        SfpApiCev.getInstance().stopForeGroundDispatch();
        super.onPause();
    }

}
